//COPYRIGHT 2020/2021- OCACHO GAMES STUDIO - DAVID MARTINEZ GARCIA
#ifndef OG_NETWORKING_H
#define OG_NETWORKING_H

#include <iostream>
#include <map>
#include <utility>
#include <functional>
#include <enet/enet.h>

/**
 * Here can be defined the events as OG_NETWORK_DATA_CHAT was defined
 * 
 */
#define OG_NETWORK_DATA_IDENTIFY 0
#define OG_NETWORK_DATA_DISCONNECT 1
#define OG_NETWORK_DATA_USER 2
#define OG_NETWORK_DATA_USERNAME 3

/*Example custom OG_NETWORK_POSITION_XY*/
#define OG_NETWORK_DATA_POSITION_XY 4

#define OG_MAX_NETWORK_DATA_MESSAGES 100

#pragma region Structs
struct og_network_peer
{
  void* enet_peer;
};

struct og_network_data{
  og_network_peer sender;
  size_t size;
  char* data;
};

struct og_network_events {
  int num_messages;
  og_network_data messages[OG_MAX_NETWORK_DATA_MESSAGES];
};

#pragma endregion

class OgNetworking
{

#pragma region Private
	private:
		og_network_events m_eventList;								//List of events (list of packets)
		ENetEvent event;											//Event handler

		ENetHost* peer;												//Our connection peer, server and client
		ENetPeer* server;											//Server peer, in case we are clients
		std::map<int, char*> m_usersMap;							//Map of users, ID and Username
		std::map<ENetPeer*, int> m_serverPeersMap;					//Auxiliar map of clients used by the server

		bool mb_isHosting;											//Whether if we are hosting or not
		bool mb_isConnected;										//Whether if we are connected or not

		int m_id;
		const char* m_username;										//Instance Username

		int m_idCounter;											//Counter for identification ID

		std::map<int, std::function<void(int,char[])>> m_hashLambdasEvents; 

		/**
		 *	Update the connect events of the engine
		 *
		 *	This function is used by the server and clients.
		 */
		void updateConnectEvent();

		/**
		 *	Update the receive events of the engine
		 *
		 *	This function is used by the server and clients.
		 */
		void updateReceiveEvent();

		/**
		 *	Update the receive identification events of the engine
		 *
		 *	This function is used by the server and clients.
		 * 	
		 * 	@param id: Incoming user ID
		 * 	@param username: Incoming user username
		 */
		void updateReceiveIdentifyEvent(int id, char username []);

		/**
		 *	Update the receive disconnection events of the engine
		 *
		 *	This function is used by the server and clients.
		 * 	
		 * 	@param id: Incoming user ID
		 * 	@param username: Incoming user username
		 */
		void updateReceiveDisconnectEvent(int id, char username []);

		/**
		 *	Update the receive new user events of the engine
		 *
		 *	This function is used by the server and clients.
		 * 	
		 * 	@param id: Incoming user ID
		 * 	@param username: Incoming user username
		 */
		void updateReceiveUserEvent(int id, char username []);

		/**
		 *	Update the disconnect events of the engine	
		 *
		 *	This function is used by the server and clients.
		 */
		void updateDisconnectEvent();

		/** 
		 * The server set the new user username
		 * The server also send the new user to all clients 
		 * 
		 * This function is used by the server
		 */
		void updateUsernameEvent(const char* p_data);

		/**
		 *	Send a packet, as server, to all your connected clients in a specific channel
		 *	The channel is set by default, no worries.
		 *
		 * 	This function is used by the server
		 *	
		 * 	@param p_data: Data you would like to send to all clients connected to you
		 * 	@param p_size: Size of p_data
		 * 	@param p_clientToAvoid: In case you want to exclude a client from sending data. Default = NULL
		 */
		void broadcastPacket(const char* p_data, size_t p_size, ENetPeer* p_clientToAvoid = NULL);

		/**
		 *	Send a packet, as client, to the server you are connected to, in a specific channel
		 *	The channel is set by default, no worries.
		 *
		 * 	This function is used by the clients
		 *	
		 * 	@param p_data: Data you would like to send to your server, as client
		 * 	@param p_size: Size of p_data
		 */
		void sendPacketToServer(const char* p_data, size_t p_size);

		/**
		 * 	Send the identification packet, as server, to a recent connected client 
		 * 
		 * 	This function is used by the server
		 * 
		 */
		void sendIdentificationPacketToClient();

		/**
		 * 	Send the disconnection packet, as server, to all the clients 
		 * 
		 * 	This function is used by the server
		 * 
		 */
		void sendDisconnectionPacketToClients();

		/**
		 * 	Send the total of current players in the game to all the clients to update them
		 * 
		 * 	This function is used by the server
		 * 
		 */
		void sendUpdateUsersMap();

#pragma endregion

#pragma region Public
	public:

		/** Constructor **/
		OgNetworking();

		/**
		 *	Initialize OG_Networking engine
		 *
		 *	@return	0 if it's succesful
		 *	@return 1 if it's not succesful
		 */
		int init();

		/**
		 *	Update the list of events (m_eventList) with the received data (packets)
		 *	Update the current players connected to the server	
		 *
		 *	This function is used by the server and clients.
		 */
		void update();

		/**
		 *	Whether the current networking instance is hosting or not
		 *
		 *	@return true = The instance is hosting
		 *	@return false = The instance is not hosting
		 */
		bool isHost()      {return mb_isHosting;}

		/**
		 *	Whether the current networking instance is connected or not
		 *
		 *	@return true = The instance is connected
		 *	@return false = The instance is not connected
		 */
		bool isConnected() {return mb_isConnected;}

		/**
		 *	Connect to the engine as host, listening for clients and hosting the game
		 *	Preparing your OC_Networking instance to be a host.
		 *	
		 * 	@param p_port: Port where you, as server, will be listening
		 * 	@param p_maxConnections: Maximum of connectioins your host supports
		 * 	@param p_username: My username
		 * 
		 *	@return Whether the operation was succesfull or not
		 */
		int createHost(unsigned int p_port, unsigned int p_maxConnections, const char* p_username);

		/**
		 *	Connect to the engine as client, trying to connect to a host address and port
		 *	Preparing your OC_Networking instance to be a client.
		 *	
		 * 	@param p_addressToConnect: Address where you, as client, would like to connect
		 * 	@param p_port: Port where you, as client, would like to connect
		 *  @param p_username: My username
		 * 
		 *	@return Whether the operation was succesfull or not
		 */
		int connect(const char* p_addressToConnecet, unsigned int port, const char* p_username);

		/**
		 * 	Send a packet to all the players
		 *	
		 * 	@param p_data: Data to send through the net
		 * 
		 */
		void sendPacket(const char* p_data);

		/**
		 *	Send a packet to a specific peer in a specific channel
		 *	The channel is set by default, no worries.
		 *
		 * 	This function is used by the server and clients
		 *	
		 * 	@param p_peer: A specific peer you want to send a packet
		 * 	@param p_data: Data you would like to send
		 * 	@param p_size: Size of p_data
		 */
		void sendPacketTo(ENetPeer* p_peer, const char* p_data, size_t p_size);

		/**
		 *	Disconnect from the server in case you are a client
		 *
		 * 	This function is used by the clients
		 */
		void disconnect();

		/**
		 *	Disconnect from the server and destroy your connection in case you are a client
		 *
		 * 	This function is used by the clients
		 */
		void cleanUp();

		/**
		 *	Your event list of your instance will be updating through the game loop
		 *	Get all the events (packet data) of your instance to loop thorugh them
		 *
		 * 	@return Current events list
		 */
		og_network_events* getEventList() {return &m_eventList;}

		/**
		 * Get the current users connected to the session [ID, username]
		 *
		 *  This function is used by the server and clients
		 *
		 * 	@return Current users connected, excluding you
		 */
		std::map<int, char*> getUsersMap() {return m_usersMap;}

		/**
		 *	Current number of players in the session
		 *
		 * 	This function is used by the server and clients
		 * 
		 *	@return Total of players connected
		 */
		int getCurrentUsersNumber() {return m_usersMap.size() + 1;}

		/**
		 *	Each peer has its own ID
		 * 
		 *	@return Instance ID
		 */
		int getId() 				{ return m_id;}

		/**
		 *	Each peer has its own username
		 * 
		 *	@return User username
		 */
		const char* getUsername()	{return m_username;}
#pragma endregion

};

#endif