cmake_minimum_required(VERSION 3.1)
project(OgNetworking)

add_library(libModulos STATIC)
add_subdirectory(code)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y -g -lenet")
add_executable(Og2Players main.cpp)

target_link_libraries(Og2Players libModulos)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake_modules")
find_package(SFML REQUIRED system window graphics network audio)
if (SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(Og2Players ${SFML_LIBRARIES})
endif()

find_package(ENet REQUIRED)
if (ENet_FOUND)
    include_directories(${ENET_INCLUDE_DIR})
    target_link_libraries(Og2Players ${ENET_LIBRARIES})
endif()
